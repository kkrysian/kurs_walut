﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Currenda.Service.ApiConnector.Models
{
    public class CurrencyRatesTable
    {
        public string Table { get; set; }

        public string Currency { get; set; }

        public string Code { get; set; }

        public List<CurrencyRate> Rates { get; set; }
    }
}