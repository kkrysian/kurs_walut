﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Currenda.Service.ApiConnector.Models
{
    public class ExchangeRatesTable 
    {
        public string Table { get; set; }

        public List<Rate> Rates { get; set; }
    }
}