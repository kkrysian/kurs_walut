﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Currenda.Service.ApiConnector.Models
{
    public class CurrencyRate
    {
        public string No { get; set; }

        public DateTime EffectiveDate { get; set; }

        public decimal Bid { get; set; }

        public decimal Ask { get; set; }
    }
}