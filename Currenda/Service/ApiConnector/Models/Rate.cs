﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Currenda.Service.ApiConnector.Models
{
    public class Rate
    {
        public string Currency { get; set; }

        public string Code { get; set; }
    }
}