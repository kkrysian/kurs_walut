﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using RestSharp;
using Currenda.Service.ApiConnector.Models;
using Currenda.Models.Db;

namespace Currenda.Service.ApiConnector
{
    public class NbpApiConnector
    {
        private const string ApiUrl = "http://api.nbp.pl";
        private const string ApiUrlCurrentTable = "api/exchangerates/tables/{tableId}";
        private const string ApiUrlCurrencyFromDateToDate = "api/exchangerates/rates/{tableId}/{code}/{startDate}/{endDate}";

        public IEnumerable<Rate> GetCurrencyCodes()
        {
            var client = new RestClient(ApiUrl);

            var request = new RestRequest(ApiUrlCurrentTable, Method.GET);
            request.AddUrlSegment("tableId", "a");

            var response = client.Execute<List<ExchangeRatesTable>>(request);
            var table = response.Data.FirstOrDefault().Rates;
            return table;
        }

        public IEnumerable<CurrencyRate> GetExchangeRates(string code, DateTime startDate, DateTime endDate)
        {
                var client = new RestClient(ApiUrl);

                var request = new RestRequest(ApiUrlCurrencyFromDateToDate, Method.GET);
                request.AddUrlSegment("tableId", "c");
                request.AddUrlSegment("code", code);
                request.AddUrlSegment("startDate", startDate.ToString("yyyy-MM-dd"));
                request.AddUrlSegment("endDate", endDate.ToString("yyyy-MM-dd"));

                var response = client.Execute<CurrencyRatesTable>(request);
                var table = response.Data.Rates;
                return table;
        }

        public bool Validate(ExchangeRate exchangeRate, out List<NbpApiValidationResult> statuses)
        {
            statuses = new List<NbpApiValidationResult>();

            if (String.IsNullOrEmpty(exchangeRate.CurrencyCode))
            {
                statuses.Add(NbpApiValidationResult.NoCode);
            }
            if (exchangeRate.StartDate == null)
            {
                statuses.Add(NbpApiValidationResult.NoStartData);
            }
            if (exchangeRate.EndDate == null)
            {
                statuses.Add(NbpApiValidationResult.NoEndData);
            }
            if (exchangeRate.EndDate < exchangeRate.StartDate)
            {
                statuses.Add(NbpApiValidationResult.EndDateIsBeforeStart);
            }
            if (exchangeRate.StartDate < new DateTime(2002, 01, 02))
            {
                statuses.Add(NbpApiValidationResult.StartDateTooOld);
            }
            if (exchangeRate.EndDate < new DateTime(2002, 01, 02))
            {
                statuses.Add(NbpApiValidationResult.EndDateTooOld);
            }
            if (exchangeRate.StartDate > System.DateTime.Now)
            {
                statuses.Add(NbpApiValidationResult.StartDateFuture);
            }
            if (exchangeRate.EndDate > System.DateTime.Now)
            {
                statuses.Add(NbpApiValidationResult.EndDateFuture);
            }

            return statuses.Any() == false;
        }
    }
}