﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Currenda.Service.ApiConnector
{
    public enum NbpApiValidationResult
    {
        Ok,
        StartDateTooOld,
        EndDateTooOld,
        StartDateFuture,
        EndDateFuture,
        EndDateIsBeforeStart,
        NoStartData,
        NoEndData,
        NoCode,
    }
}