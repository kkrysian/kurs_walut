﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Currenda.Service.Calculator
{
    public class RateSummary
    {
        public RateSummary(decimal avarage, decimal standardDeviation)
        {
            Average = avarage;
            StandardDeviation = standardDeviation;
        }

        public decimal Average { get; private set; }

        public decimal StandardDeviation { get; private set; }
    }
}