﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Currenda.Service.ApiConnector.Models;
using Currenda.Helper;

namespace Currenda.Service.Calculator
{
    public class RateCalculator
    {
        private IEnumerable<CurrencyRate> allCurrencyCode;

        public RateCalculator(IEnumerable<CurrencyRate> allCurrencyCode)
        {
            this.allCurrencyCode = allCurrencyCode;
        }

        public RateSummary Calculate()
        {
            var average = Math.Round(allCurrencyCode.Select(a => a.Bid).Average(), 4);
            var standardDeviation = Math.Round(allCurrencyCode.Select(a => a.Ask).StandardDeviation(), 4);

            var result = new RateSummary(average, standardDeviation);

            return result;
        }
    }
}
