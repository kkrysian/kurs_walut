﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Currenda.Models.Db;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Reflection;

namespace Currenda.Service.DataService
{
    public class ExchangeRateXmlService
    {
        public void CreateExchange(ExchangeRate exchangeRate)
        {
            var xs = new XmlSerializer(typeof(ExchangeRate));
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data\\Currenda.xml");

            using (var sww = new StreamWriter(path, true))
            using (var writer = XmlWriter.Create(sww))
            {
                xs.Serialize(writer, exchangeRate);
            }
        }
    }
}