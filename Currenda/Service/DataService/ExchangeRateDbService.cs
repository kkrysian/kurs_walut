﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Currenda.Models.Db;

namespace Currenda.Service.DataService
{
    public class ExchangeRateDbService : IDisposable
    {
        private CurrendaDbContext db;

        public ExchangeRateDbService()
        {
            db = new CurrendaDbContext();
        }

        public void CreateExchange(ExchangeRate exchangeRate)
        {
            db.ExchangeRate.Add(exchangeRate);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}