﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using System.Collections;
using Currenda.Models.Db;
using Currenda.Helper;
using Currenda.Service.ApiConnector;
using Currenda.Service.Calculator;
using Currenda.Service.ApiConnector.Models;
using System.ComponentModel.DataAnnotations;
using Currenda.Service.DataService;
using NLog;
using RestSharp.Serializers;

namespace Currenda.Controllers
{
    public class HomeController : Controller
    {
        private NbpApiConnector connector = new NbpApiConnector();

        private Logger logger = LogManager.GetCurrentClassLogger();

        private static IEnumerable<Rate> currencyCodes = new List<Rate>();

        private IEnumerable<Rate> ShowCurrencyCode
        {
            get
            {
                if (!currencyCodes.Any())
                {
                    logger.Info("Pobieranie kodów walut.");
                    currencyCodes = connector.GetCurrencyCodes();
                    logger.Info("Kody walut pobrane: " + currencyCodes.Count());
                }

                return currencyCodes;
            }
        }
        
        public ActionResult Index()
        {
            try
            {
                ViewBag._currencyCode = new SelectList(ShowCurrencyCode, "Code", "Currency");
            }
            catch (ArgumentNullException)
            {
                logger.Info("Brak połączenia z NBP.");
                return View("Error");
            }

            return View();
        }
        
        [HttpPost]
        public ActionResult Index(ExchangeRate model)
        {
            ViewBag._currencyCode = new SelectList(ShowCurrencyCode, "Code", "Currency");
            
            List<NbpApiValidationResult> statuses;
            if (!connector.Validate(model, out statuses))
            {
                this.ShowValidationMessages(statuses);
                return View(model);
            }
            try
            {
                var currencyValues = connector.GetExchangeRates(model.CurrencyCode, model.StartDate, model.EndDate);
                var calculator = new RateCalculator(currencyValues);
                var exchangeCalculate = calculator.Calculate();

                model.CreatedDate = System.DateTime.Now;
                model.AverageBuyingRate = exchangeCalculate.Average;
                model.StandardDeviation = exchangeCalculate.StandardDeviation;

                logger.Info("Kursy walut pobrane: " + new JsonSerializer().Serialize(model));

                SaveToDB(model);
                SaveToXML(model);
            }
            catch (NullReferenceException)
            {
                logger.Info("Zapytanie o walutę bez odpowiedzi.");
                return View("Info");
            }

            return View(model);
        }
        
        private void SaveToDB(ExchangeRate model)
        {
            using (var service = new ExchangeRateDbService())
            {
                service.CreateExchange(model);
            } 
        }

        private void SaveToXML(ExchangeRate model)
        {
            new ExchangeRateXmlService().CreateExchange(model);
        }
        
        private void ShowValidationMessages(List<NbpApiValidationResult> statuses)
        {
            if (statuses.Contains(NbpApiValidationResult.NoCode))
            {
                ModelState.AddModelError("CurrencyCode", "Wybranie waluty jest wymagane");
            }
            if (statuses.Contains(NbpApiValidationResult.NoStartData))
            {
                ModelState.AddModelError("StartDate", "Wybranie daty początkowej jest wymagane");
            }
            if (statuses.Contains(NbpApiValidationResult.NoEndData))
            {
                ModelState.AddModelError("EndDate", "Wybranie daty końcowej jest wymagane");
            }
            if (statuses.Contains(NbpApiValidationResult.EndDateIsBeforeStart))
            {
                ModelState.AddModelError("StartDate", "Data końcowa nie może być przed datą początkową");
                ModelState.AddModelError("EndDate", "Data końcowa nie może być przed datą początkową");
            }
            if (statuses.Contains(NbpApiValidationResult.StartDateTooOld))
            {
                ModelState.AddModelError("StartDate", "NBP nie umożliwia pobrania danych sprzed 02.01.2002r.");
            }
            if (statuses.Contains(NbpApiValidationResult.EndDateTooOld))
            {
                ModelState.AddModelError("EndDate", "NBP nie umożliwia pobrania danych sprzed 02.01.2002r.");
            }
            if (statuses.Contains(NbpApiValidationResult.StartDateFuture))
            {
                ModelState.AddModelError("StartDate", "NBP umożliwia pobranie danych z przeszłości");
            }
            if (statuses.Contains(NbpApiValidationResult.EndDateFuture))
            {
                ModelState.AddModelError("EndDate", "NBP umożliwia pobranie danych z przeszłości");
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
