﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Currenda.Models.Db
{
    public class CurrendaDbContext : DbContext
    {
        public DbSet<ExchangeRate> ExchangeRate { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExchangeRate>().Property(a => a.AverageBuyingRate).HasPrecision(18, 4);
            modelBuilder.Entity<ExchangeRate>().Property(a => a.StandardDeviation).HasPrecision(18, 4);
            base.OnModelCreating(modelBuilder);
        }
    }
}