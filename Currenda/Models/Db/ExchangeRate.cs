﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace Currenda.Models.Db
{
    public class ExchangeRate
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Wprowadzenie kodu waluty jest wymagane")]
        [Display(Name = "Kod waluty")]
        [StringLength(30, ErrorMessage = "Błędny kod waluty", MinimumLength = 3)]
        public string CurrencyCode { get; set; }
        
        [Required(ErrorMessage = "Wprowadzenie daty początkowej jest wymagane")]
        [UIHint("DateEmpty")]
        [DataType(DataType.Date)]
        [Display(Name = "Data początkowa")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "Wprowadzenie daty końcowej jest wymagane")]
        [UIHint("DateEmpty")]
        [DataType(DataType.Date)]
        [Display(Name = "Data końcowa")]
        public DateTime EndDate { get; set; }

        [Required]
        [Display(Name = "Średni kurs kupna")]
        [DisplayFormat(DataFormatString = "{0:N4}",ApplyFormatInEditMode = true)]
        public decimal AverageBuyingRate { get; set; }

        [Required]
        [Display(Name = "Odchylenie standardowe")]
        [DisplayFormat(DataFormatString = "{0:N4}", ApplyFormatInEditMode = true)]
        public decimal StandardDeviation { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Data zapisu")]
        public DateTime CreatedDate { get; set; }
    }
}
