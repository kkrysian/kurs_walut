﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Currenda.Helper
{
    public static class MathExtensions
    {
        public static decimal StandardDeviation(this IEnumerable<decimal> values)
        {
          decimal ret = 0;
          if (values.Count() > 0) 
          {           
             var avg = values.Average();
             var sum = values.Sum(d => (d - avg) * (d - avg));    
             ret = (decimal)Math.Sqrt((double)sum / values.Count());   
          }   
          return ret;
        }
    }
}
